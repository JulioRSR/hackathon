import { LitElement, html } from "lit"

class RatingFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <footer class="mt-4">
                <p>Rating App ${this.getYear()}</p>
            </footer>
        `;
    }

    getYear() {
        let year = new Date().getFullYear();
        return year;
    }
}

customElements.define('rating-footer', RatingFooter);