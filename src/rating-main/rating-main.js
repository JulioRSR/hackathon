import { LitElement, html } from "lit"
import '../rating-dm/rating-dm.js';
import '../rating-card/rating-card.js';

class RatingMain extends LitElement {

    static get properties() {
        return {
            products: {type: Array}
        };
    }

    constructor() {
        super();

        this.products = [];
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <main>
                <div class="container">
                    <div class="row">
                        ${this.products.map(
                            product => html`
                            <div class="col-md-3 mb-4">    
                                <rating-card 
                                    id="rating-card" 
                                    .id="${product.id}"
                                    .title="${product.title}"
                                    .description="${product.description}"
                                    .image="${product.image}"
                                    .url="${product.url}"
                                    .team="${product.team}"
                                    .rate="${product.rate}"
                                    @post-product-rate="${this.postProductRate}"
                                    >
                                </rating-card>
                            </div>
                            `
                        )}
                    </div>
                    <rating-dm id="dm" @get-products-data="${this.getProductsData}"></rating-dm>
                </div>
            </main>
        `;
    } 

    updated(changedProperties) {
        // TODO
    }

    getProductsData(e) {
        this.products = e.detail.products;
    }

    postProductRate(e) {
        this.shadowRoot.getElementById('dm').rateData = e.detail;
    }
}

customElements.define('rating-main', RatingMain);