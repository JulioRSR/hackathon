import { LitElement, html } from "lit"
import '../rating-header/rating-header.js';
import '../rating-main/rating-main.js';
import '../rating-footer/rating-footer.js';

class RatingApp extends LitElement {

    static get properties() {
        return {
            products: {type: Array}
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <rating-header></rating-header>
            <div class="container-fluid">
                <div class="row">
                    <rating-main></rating-main>
                </div>
            </div>
            <rating-footer></rating-footer>
        `;
    }

    updated(changedProperties) {
    }
}

customElements.define('rating-app', RatingApp);