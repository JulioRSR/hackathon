import { LitElement, html } from "lit"

class RatingForm extends LitElement {

    static get properties() {
        return {
            id: {type: Number}
        };
    }

    constructor() {
        super();

        this.id = '';
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <form>
                <select class="form-select" @change="${this.postProductRate}">
                    <option selected>Valora</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </form>
        `;
    }

    updated(changedProperties) {
    }

    postProductRate(e) {
        e.preventDefault();
        e.target.setAttribute('disabled', 'disabled');
        this.dispatchEvent(
            new CustomEvent(
                'post-product-rate-form', {
                    detail: {
                        id: this.id,
                        rate: parseInt(e.target.value)
                    }
                }
            )
        );
    }
}

customElements.define('rating-form', RatingForm);