import { LitElement, html } from "lit"

class RatingDM extends LitElement {

    static get properties() {
        return {
            products: {type: Array},
            product: {type: Object},
            urlAPI: {type: String},
            rateData: {type: Object}
        };
    }

    constructor() {
        super();

        this.urlAPI = 'https://guarded-waters-66204.herokuapp.com/rateAppAPI/v1/';

        this.products = [];
        this.product = {};
        this.rateData = {id: '', rate: ''};
        this.getProductsDataAPI();
    }

    updated(changedProperties) {
        if (changedProperties.has('products')) {
            this.dispatchEvent(
                new CustomEvent(
                    'get-products-data',
                    {
                        detail: {
                            products: this.products
                        }
                    }
                )
            );
        }
        if (changedProperties.has('rateData')) {
            this.postProductRate(this.rateData.id, this.rateData.rate);
        }
    }

    getProductsDataAPI() {
        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIresponse = JSON.parse(xhr.responseText);
                this.products = APIresponse;
            }
        }
        xhr.open('GET', this.urlAPI + 'products');
        xhr.send();
    }

    getProductDataAPIById(id) {
        if (id > 0) {
            let xhr = new XMLHttpRequest();
            xhr.onload = () => {
                if (xhr.status === 200) {
                    let APIresponse = JSON.parse(xhr.responseText);
                    this.product = APIresponse;
                }
            }
            xhr.open('GET', this.urlAPI + 'products/' + id);
            xhr.send();
        }
        
        /**
         * TODO GET request
         * llamamos al metodo de la api que nos devuelva el objeto producto por id
         * cuando nos devuelva buscamos en nuestro objeto this.productos ese id y actualizamos ese indice del array
         * this.products buscas el que tiene el id igual que el id que pasas por parametro y localizas el indice para actualizarlo y por el mismo cableado llega a rate-card 
        */ 
    }

    postProductRate(id, rate) {
        if (id > 0 && rate > 0) {
            let xhr = new XMLHttpRequest();
            let params = JSON.stringify({id:+id, rate: rate});
            xhr.open('PATCH', this.urlAPI + 'products/' + id, false);
            xhr.setRequestHeader("Content-type","application/json");
            xhr.send(params);
            xhr.onload = () => {
                if (xhr.status === 200) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

customElements.define('rating-dm', RatingDM);