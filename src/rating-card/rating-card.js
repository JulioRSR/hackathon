import { LitElement, html } from "lit"
import '../rating-form/rating-form.js';

class RatingCard extends LitElement {

    static get properties() {
        return {
            id: {type: Number},
            title: {type: String},
            description: {type: String},
            image: {type: String},
            url: {type: String},
            team: {type: String},
            rate: {type: Number}
        };
    }

    constructor() {
        super();

        this.id = '';
        this.title = '';
        this.description = '';
        this.image = '';
        this.url = '';
        this.team = '';
        this.rate = '';
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.image}" class="card-img-top" alt="${this.title} photo">
                <div class="card-body">
                    <h4 class="card-title">${this.title}</h4><p><span class="badge bg-secondary text-light">${this.team}</span></p>
                    <p class="card-text">${this.description}</p>
                    <div class="row">
                        <div class="col col-md-6 mb-2">
                            <a href="${this.url}" class="btn btn-primary d-grid gap-2" target="_blank">Visit App</a>
                        </div>
                        <div class="col col-md-6">
                            <rating-form id="form-rating" @post-product-rate-form="${this.postProductRate}"></rating-form>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    Total: <span class="badge bg-primary">${this.rate}</span>
                </div>
            </div>
        `;
    }

    updated(changedProperties) {
        if (changedProperties.has('id')) {
            this.shadowRoot.getElementById('form-rating').id = this.id;
        }
    }

    postProductRate(e) {
        e.preventDefault();
        this.dispatchEvent(
            new CustomEvent(
                'post-product-rate', {
                    detail: {
                        id: e.detail.id,
                        rate: e.detail.rate
                    }
                }
            )
        );
    }
}

customElements.define('rating-card', RatingCard);